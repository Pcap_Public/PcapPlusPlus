//// ReadingWritingPCAP.cpp : Defines the entry point for the console application.
////
//
//#include "stdlib.h"
//#include "Packet.h"
//#include "TcpLayer.h"
//#include "PcapFileDevice.h"
//#include <PcapFileDevice.h>
//
///**
//* main method of the application
//*/
//int main(int argc, char* argv[])
//{
//	// use the IFileReaderDevice interface to automatically identify file type (pcap/pcap-ng)
//	// and create an interface instance that both readers implement
//	pcpp::IFileReaderDevice* reader = pcpp::IFileReaderDevice::getReader("001_ADS-B_Cat021(v0.26)_Data_01.pcap");
//
//	// verify that a reader interface was indeed created
//	if (reader == NULL)
//	{
//		printf("Cannot determine reader for file type\n");
//		system("PAUSE");
//		exit(1);
//	}
//
//	// open the reader for reading
//	if (!reader->open())
//	{
//		printf("Cannot open input.pcap for reading\n");
//		system("PAUSE");
//		exit(1);
//	}
//
//	// create a pcap file writer. Specify file name and link type of all packets that
//	// will be written to it
//	pcpp::PcapFileWriterDevice pcapWriter("001_ADS-B_Cat021(v0.26)_Data_01_Modified.pcap", pcpp::LINKTYPE_ETHERNET);
//
//	// try to open the file for writing
//	if (!pcapWriter.open())
//	{
//		printf("Cannot open 001_ADS-B_Cat021(v0.26)_Data_01_Modified.pcap for writing\n");
//		system("PAUSE");
//		exit(1);
//	}
//
//	// set a BPF filter for the reader - only packets that match the filter will be read
//	//if (!reader->setFilter("net 98.138.19.88"))
//	//{
//	//	printf("Cannot set filter for file reader\n");
//	//	system("PAUSE");
//	//	exit(1);
//	//}
//
//	// the packet container
//	pcpp::RawPacket rawPacket;
//	int PacketNum = 0;
//
//	// a while loop that will continue as long as there are packets in the input file
//	// matching the BPF filter
//	while (reader->getNextPacket(rawPacket))
//	{
//		// parse the raw packet into a parsed packet
//		pcpp::Packet parsedPacket(&rawPacket);
//		// let's get the TCP layer
//		pcpp::TcpLayer* tcpLayer = parsedPacket.getLayerOfType<pcpp::TcpLayer>();
//
//
//		uint8_t tempData[4] = {1,1,1,1};
//		pcpp::RawPacket rawPacketTemp = rawPacket;
//		rawPacketTemp.setRawData(&tempData[0],4, rawPacketTemp.getPacketTimeStamp());
//		// write each packet to both writers
//		pcapWriter.writePacket(rawPacketTemp);
//		printf("PacketNum: %d\n", ++PacketNum);
//	}
//
//	// create the stats object
//	pcap_stat stats;
//
//	// read stats from reader and print them
//	reader->getStatistics(stats);
//	printf("Read %d packets successfully and %d packets could not be read\n", stats.ps_recv, stats.ps_drop);
//
//	// read stats from pcap writer and print them
//	pcapWriter.getStatistics(stats);
//	printf("Written %d packets successfully to pcap writer and %d packets could not be written\n", stats.ps_recv, stats.ps_drop);
//
//	// close reader
//	reader->close();
//
//	// close writers
//	pcapWriter.close();
//
//	// free reader memory because it was created by pcpp::IFileReaderDevice::getReader()
//	delete reader;
//
//	system("PAUSE");
//
//}
