PCAPPLUSPLUS_HOME := E:/01_JSKJ/03_Develop/01_Projects/11_AIVis/01_Trunk/01_Codes/ThirdParty/Pcap/PcapPlusPlus/PcapPlusPlus-17.11
 
MINGW_HOME := C:/mingw-w64/i686-8.1.0-posix-dwarf-rt_v6-rev0/mingw32/i686-w64-mingw32
 
WINPCAP_HOME := C:/WinPcap
 
MSYS_HOME := C:/msys32
 
### COMMON ###

# includes
PCAPPP_INCLUDES := -I$(PCAPPLUSPLUS_HOME)/Dist/header

# libs dir
PCAPPP_LIBS_DIR := -L$(PCAPPLUSPLUS_HOME)/Dist

# libs
PCAPPP_LIBS := -lPcap++ -lPacket++ -lCommon++

# post build
PCAPPP_POST_BUILD :=

# build flags
PCAPPP_BUILD_FLAGS :=

